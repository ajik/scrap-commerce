package id.scrap.com;

import com.fasterxml.jackson.core.util.DefaultIndenter;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.NicelyResynchronizingAjaxController;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.FileWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ScrapApplication {

    private static final String url = "https://www.tokopedia.com/p/handphone-tablet/handphone";
    private static final String getXPathListProductContainer = ".//div[@data-testid='lstCL2ProductList']/div[contains(@class,'css-bk6tz')]";
    private static final String getXPathImageLink = ".//div[contains(@class, 'css-79elbk')]/div/div/img";
    private static final String getXPathProductName = ".//a/div[@data-testid='divProductWrapper']/div[contains(@class, 'css-11s9vse')]/span";
    private static final String getXPathPrice = ".//a/div[@data-testid='divProductWrapper']/div[contains(@class, 'css-11s9vse')]/div/div/span";
    private static final String getXPathMerchantName = ".//a/div[@data-testid='divProductWrapper']/div[contains(@class, 'css-11s9vse')]/div[contains(@class, 'css-tpww51')]/div/span";
    private static final String getXPathRating = ".//a/div[@data-testid='divProductWrapper']/div[contains(@class, 'css-11s9vse')]/div[contains(@class, 'css-153qjw7')]/div/img";
    private static final String starYellow = "https://assets.tokopedia.net/assets-tokopedia-lite/v2/zeus/kratos/4fede911.svg";
    private static final String getXPathNextButton = ".//button[contains(@class, 'e19tp72t3') and @aria-label='Halaman berikutnya']";

    public static void main(String[] args) {
        ScrapApplication sa = new ScrapApplication();
        sa.webScrapV2(url);
    }

    public void webScrapV2(String url) {
        try {
            System.setProperty("webdriver.chrome.driver", "D://workspace//java//scrap-tokopedia//chromedriver_2.exe");
            ChromeOptions opt = new ChromeOptions();
            //opt.setPageLoadStrategy(PageLoadStrategy.NORMAL);
            ChromeDriver driver = new ChromeDriver(opt);

            driver.navigate().to(url);

            JavascriptExecutor jsExec = (JavascriptExecutor) driver;
            jsExec.executeScript("window.scrollBy(0,250)");

            WebDriverWait wait = new WebDriverWait(driver, 5000);

            List<WebElement> listProduct = driver.findElementsByXPath(getXPathListProductContainer);
            int productSize = listProduct.size();
            List<Smartphone> smartphoneList = getSmartphone(driver, listProduct, new ArrayList<Smartphone>(), jsExec, 0);

            for(Smartphone sp: smartphoneList) {
                System.out.print(sp.getNameOfProduct() + ", harga: " + sp.getPrice());
                System.out.println();
            }
            System.out.println(smartphoneList.size());

            convertToCsv(smartphoneList);

        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }

    }

    public List<Smartphone> getSmartphone(ChromeDriver driver, List<WebElement> listProduct, List<Smartphone> listSmartphone, JavascriptExecutor jsExec, int total) throws Exception {
        int size = listProduct.size();
        if(listSmartphone == null) listSmartphone = new ArrayList<Smartphone>();
        if(listSmartphone.size() > 100) return listSmartphone;
        if(total > 5) return listSmartphone;
        for (int i = 0; i < size; i++) {
            if(listSmartphone.size() > 100) break;
            Smartphone sp = null;
            WebElement product = listProduct.get(i);
            WebElement productAnchor = product.findElement(By.xpath(".//a"));
            String href = productAnchor.getAttribute("href");
            String[] params = new URL(href).getQuery().split("&");
            WebElement img = product.findElement(By.xpath(getXPathImageLink));
            String imgLink = img.getAttribute("src");
            String urlProduct = "";
            for(String s: params) {
                String[] arr = s.split("=");
                if(arr[0].equals("r")) {
                    urlProduct = arr[1];
                    break;
                }
            }

            WebElement priceDiv = product.findElement(By.xpath(getXPathPrice));
            WebElement productNameHeading = product.findElement(By.xpath(getXPathProductName));
            WebElement merchantNameHeading = product.findElement(By.xpath(getXPathMerchantName));
            List<WebElement> ratingSpan = product.findElements(By.xpath(getXPathRating));

            String productName = productNameHeading.getText();
            String merchantName = merchantNameHeading.getText();
            String price = priceDiv.getText();
            int rating = 0;
            for(WebElement we: ratingSpan) {
                String asset = we.getAttribute("src");
                if(asset.equals(starYellow)) {
                    rating += 1;
                }
            }

            sp = new Smartphone(productName, "", imgLink, price, String.valueOf(rating) + " out of 5 stars", merchantName);
            listSmartphone.add(sp);
        }

        //return listSmartphone;

        if(listSmartphone.size() > 100) {
            return listSmartphone;
        } else {
            //jsExec.executeScript("window.scrollBy(0,4500)");
            try {
                WebElement nextButton = driver.findElementByXPath(getXPathNextButton);
                jsExec.executeScript("arguments[0].click()", nextButton);
            } catch(Exception e) { }

            jsExec.executeScript("window.scrollBy(0,250)");
            listProduct = driver.findElementsByXPath(getXPathListProductContainer);
            return getSmartphone(driver, listProduct, listSmartphone, jsExec, ++total);
        }
    }

    public static DefaultPrettyPrinter formatJsonString() throws Exception {
        DefaultPrettyPrinter.Indenter indenter = new DefaultIndenter("    ", DefaultIndenter.SYS_LF);
        DefaultPrettyPrinter printer = new DefaultPrettyPrinter();
        printer.indentObjectsWith(indenter);
        printer.indentArraysWith(indenter);

        return printer;
    }

    public static void convertToCsv(List<Smartphone> list) throws Exception {
        System.out.println("Starting write and create json file");
        File myObj = new File("web-scrapping.csv");
        if (myObj.createNewFile()) {
            System.out.println("File created: " + myObj.getName());
        } else {
            System.out.println("Delete file first!");
            myObj.delete();
            myObj.createNewFile();
        }

        StringBuilder data = new StringBuilder("data:text/csv;charset=utf-8,\"Name of Product\", \"Description\", \"Image Link\", \"Price\", \"Rating (out of 5 stars)\", \"Name of Stor or Merchant\"\n");
        for(Smartphone sp: list) {
            data.append("\"").append(sp.getNameOfProduct()).append("\",");
            data.append("\"").append(sp.getDescription()).append("\",");
            data.append("\"").append(sp.getImageLink()).append("\",");
            data.append("\"").append(sp.getPrice()).append("\",");
            data.append("\"").append(sp.getRating()).append("\",");
            data.append("\"").append(sp.getNameOfMerchant()).append("\"");
            data.append("\n");
        }

        FileWriter myWriter = new FileWriter("web-scrapping.csv");
        myWriter.write(data.toString());
        System.out.println("File writer has finished!");
        myWriter.close();
    }
}