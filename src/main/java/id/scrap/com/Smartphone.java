package id.scrap.com;

public class Smartphone {

    private String nameOfProduct;
    private String description;
    private String imageLink;
    private String price;
    private String rating;
    private String nameOfMerchant;

    public Smartphone(String nameOfProduct, String description, String imageLink, String price, String rating, String nameOfMerchant) {
        this.nameOfProduct = nameOfProduct;
        this.description = description;
        this.imageLink = imageLink;
        this.price = price;
        this.rating = rating;
        this.nameOfMerchant = nameOfMerchant;
    }

    public String getNameOfProduct() {
        return nameOfProduct;
    }

    public String getDescription() {
        return description;
    }

    public String getImageLink() {
        return imageLink;
    }

    public String getPrice() {
        return price;
    }

    public Long getPriceNumber() {
        if(this.price != null) {
            return Long.valueOf(this.price.replace("Rp", "").replace(".", ""));
        } else {
            return null;
        }
    }

    public String getRating() {
        return rating;
    }

    public String getNameOfMerchant() {
        return nameOfMerchant;
    }
}
